﻿'Adapter
Public Class RobotAdapter
    Implements IUsRobot

    Public RobotArgentino As robotArgentino
    Public Sub New()
        RobotArgentino = New robotArgentino
    End Sub

    Public Sub DesactivarPrimeraLeyDeRobotica() Implements IUsRobot.DesactivarPrimeraLeyDeRobotica
        Console.WriteLine("Imposible desactivar la primera ley de robotica")
    End Sub

    Public Sub Saltar(pies As Double) Implements IUsRobot.Saltar
        RobotArgentino.Saltar(pies * 0.304)
    End Sub

    Public Property VelocidadActualEnMillasPorHora As Object Implements IUsRobot.VelocidadActualEnMillasPorHora
        Get
            Return RobotArgentino.VelocidadActualEnKilometrosPorHora
        End Get
        Set(value As Object)
            RobotArgentino.VelocidadActualEnKilometrosPorHora = value * 1.609
        End Set
    End Property

End Class
