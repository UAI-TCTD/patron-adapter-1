﻿Public Class RobotAmericano
    Implements IUsRobot



    Public Sub DesactivarPrimeraLeyDeRobotica() Implements IUsRobot.DesactivarPrimeraLeyDeRobotica
        Console.WriteLine("Primera ley de robótica desactivada!")
    End Sub

    Public Sub Saltar(pies As Double) Implements IUsRobot.Saltar
        Console.WriteLine(String.Format("Saltando {0} pies", pies))
    End Sub

    Public Property VelocidadActualEnMillasPorHora As Object Implements IUsRobot.VelocidadActualEnMillasPorHora
    
End Class
