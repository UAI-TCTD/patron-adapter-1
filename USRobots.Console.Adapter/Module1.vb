﻿Module Module1

    Sub Main()


        Dim robotAmericano As New RobotAmericano

        robotAmericano.Saltar(100)
        robotAmericano.DesactivarPrimeraLeyDeRobotica()
        robotAmericano.VelocidadActualEnMillasPorHora = 100
        Console.WriteLine(String.Format("Velocidad actual es {0} m/h", robotAmericano.VelocidadActualEnMillasPorHora))

        Dim robotArgento As New RobotAdapter
        robotArgento.Saltar(100)
        robotArgento.DesactivarPrimeraLeyDeRobotica()
        robotArgento.VelocidadActualEnMillasPorHora = 100

        Console.WriteLine(String.Format("Velocidad actual es {0} km/h", robotArgento.VelocidadActualEnMillasPorHora))
        Console.ReadKey()
    End Sub

End Module
